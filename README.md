# checkstyle-bic
Включает себя сборку [правил](http://192.168.1.70:8888/Infrastructure/checkstyle-bic/blob/master/configs/bic_checks.xml) checkstyle, на основе [Google's style](http://checkstyle.sourceforge.net/google_style.html), также позволяет использовать правила из [sevntu-checkstyle](http://sevntu-checkstyle.github.io/sevntu.checkstyle/#ExamplesOfUsage)
## Usage
1. Подключить [BIC.xml](http://192.168.1.70:8888/efremov/intellij-settings/raw/master/codestyles/BIC.xml) ([how-to](https://www.jetbrains.com/help/idea/settings-code-style.html)) - для корректной работы автоформатирования
2. Подключить [CheckStyle-IDEA plugin](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea) ([how-to](https://www.jetbrains.com/help/idea/installing-updating-and-uninstalling-repository-plugins.html)) - для подсветки ошибок в Intellij
3. Установить версию checkstyle на **8.39**
4. Добавить в плагин sevntu-checkstyle библиотеку из node_modules/checkstyle-bic/checkstyle/sevntu-checkstyle-idea-extension-${VERSION}.jar ([how-to](https://github.com/sevntu-checkstyle/sevntu.checkstyle/wiki/How-to-use-SevNTU-Checkstyle-in-Intellij-IDEA))
5. Настроить плагин на использование конфига из папки node_modules/checkstyle-bic/config/bic_checks.xml
6. project.dir="$PATH_TO_PROJECT_ROOT", user.dir="$PATH_TO_PROJECT_ROOT/octopus-webapp/node_modules/checkstyle-bic/"
6. Поместить файл для генерации equals и hashcode [equalsHashCodeTemplates.xml](http://192.168.1.70:8888/efremov/intellij-settings/raw/5597938ed027896381ef2695801d302eee0c9a7b/equalsHashCodeTemplates.xml) в /home/user/.IntelliJIdea2018.1/config/options/
