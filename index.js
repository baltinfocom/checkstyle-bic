const { exec } = require('child_process');
const os = require('os');

const CHECKSTYLE_VERSION = '8.39';
const SEVNTU_CHECKSTYLE_VERSION = '1.38.0';
// const QULICE_CHECKSTYLE_VERSION = '0.17.3';

const checkstyle = (filePaths, cwd) => {
	const quotedPaths = filePaths.map(path => `"${path}"`);
	return new Promise((resolve, reject) => {
		const separator = os.platform() === 'win32' ? ';' : ':';
		const command = `java -Duser.dir="${__dirname}" -Dproject.dir=${process.cwd()}/${cwd} -cp "checkstyle/sevntu-checkstyle-idea-extension-${SEVNTU_CHECKSTYLE_VERSION}.jar"${separator}`
			// + `${__dirname}/checkstyle/qulice-checkstyle-${QULICE_CHECKSTYLE_VERSION}.jar${separator}`
			+ `"checkstyle/checkstyle-${CHECKSTYLE_VERSION}-all.jar" `
			+ `com.puppycrawl.tools.checkstyle.Main -d -c "${__dirname}/configs/bic_checks.xml" ${quotedPaths.join(' ')}`;
		exec(
			command,
			{ cwd }, (error, stdout) => {
				if (error) {
					// 18 - is length of stdout without errors, which means that checkstyle was failed on internal error
					reject(stdout.length === 18 ? error : stdout);
				} else {
					resolve();
				}
			},
		);
	});
};

module.exports = checkstyle;
